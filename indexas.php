<?php 
require_once('/commom/database.php');
require_once('/commom/common.php');
require_once('/commom/functions.php');

$feedData=$feed->newsFeed();

// check that the 'registered' key exists
if(isset($_SESSION['admin_id']))
{
   $session = $_SESSION['admin_id'];


?>

$feedData=$feed->newsFeed();
?>
<!DOCTYPE html >
<html>
<head>
<!-- JavaScript CSS -->
<link rel="stylesheet" type="text/css" href="css/tooltipsterReaction.css">
<link rel="stylesheet" type="text/css" href="css/tipsy.css">
<link rel="stylesheet" type="text/css" href="css/app.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery.livequery.js"></script>
<script src="js/jquery.tooltipsterReaction.js"></script>
<script src="js/jquery.tipsy.js"></script>
<script src="js/app.js"></script>
<!-- JavaScript CSS End -->
</head>
<body>
<?php
foreach($feedData as $data)
{
?>
<div class="messageBody" id="msg<?php echo $data->app_id; ?>">

<b><a href="<?php echo BASE_URL.$data->app_id; ?>"><?php echo $data->app_nome; ?></a></b>
<?php echo $data->app_descricao; ?>
<div class="messageFooter">
<a href="#" class="reaction" id="like<?php echo $data->app_id; ?>" rel="like">
<i class="likeIconDefault" ></i>Like</a>
</div>
</div>
<?php } ?>


<?php
}
?>
<script>


/*Reaction*/
$("body").on("click",".likeTypeAction",function()
{
var reactionType=$(this).attr("data-reaction");
var reactionName=$(this).attr("original-title");
var rel=$(this).parent().parent().attr("rel");
var x=$(this). parent().parent().attr("id");
var sid=x.split("reaction");
var msg_id=sid[1];

var htmlData='<i class="'+reactionName.toLowerCase()+'IconSmall likeTypeSmall" ></i>'+reactionName+'</a>';
var dataString = 'msg_id='+ msg_id +'&rid='+reactionType;

$.ajax({
type: "POST",
url: 'ajaxReaction.php',
data: dataString,
cache: false,
beforeSend: function(){},
success: function(html)
{ 
if(parseInt(html)==1)
{ 
$("#like"+msg_id).html(htmlData).removeClass('reaction').removeClass('tooltipstered').addClass('unLike').attr('rel','unlike');
$("#"+x).hide();
}
}
});

return false;
});

</script>


<script>

$("body").on("click",".unLike",function()
{
var reactionType='1';
var x=$(this).attr("id");
var
d=x.split("like");
var msg_id=sid[1];
var dataString = 'msg_id='+ msg_id +'&rid='+reactionType;
var htmlData='<i class="likeIconDefault" ></i>Like</a>';
$.ajax({
type: "POST",
url: 'ajaxReaction.php',
data: dataString,
cache: false,
beforeSend: function(){},
success: function(html)
{
if(parseInt(html)==2)
{ 
$("#like"+msg_id).html(htmlData).addClass('reaction').addClass('tooltipstered').removeClass('unLike');
}
}
});

return false;
});


</script>




</body> 
</html> 