<script type="text/javascript">
  (function ($, W, D)
  {
  var JQUERY4U = {};
  JQUERY4U.UTIL =
      {
          setupFormValidation: function ()
          {
          //form validation rules
          $("#myForm").validate({
              rules: {
              quantidade: "required",
              /*url: {
                  required: true,
                  url: true
              },*/, 
			  /*
              phone: {
                      required: true,
                       minlength: 10,
                       pattern: [/^[7-9]{1}[0-9]{9}$/], 
                  },*/
              /*email: {
                      required: true,
                      email: true
                  },*/
              /*
			  */description: {
                      required: true,
                      minlength: 20
                  },
              firstname:{
                      required: true,
                      minlength: 8
                  },
              date: {
                  
                  required:true,
                  date: true
                  }
              },
              messages: {
              quantidade: "Please select category",
              firstname: "Please insert name of your app",
              date: "Please Enter valide date",
              description: "Please insert description",
              },
              submitHandler: function (form) {
              form.submit();
              }
          });
        }
      }
  //when the dom has loaded setup form validation rules
  $(D).ready(function ($) {
      JQUERY4U.UTIL.setupFormValidation();
  });
  })(jQuery, window, document);
</script>