<script>


// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='registration']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
	  quantidade: {
	  required: true
	  },
      firstname: "required",
      /*lastname: "required",
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },*/
      date: {
        required: true,
        
      },
	  description: {
	  required: true,
	  minlength: 10
	  }
    },
    // Specify validation error messages
    messages: {
		quantidade : "Please select one category of your app",
      firstname: "Please enter the name of your app",
      //lastname: "Please enter your lastname",
      description: {
        required: "Please provide a description",
        minlength: "Your description must be at least 10 characters long"
      },
      date: "Please enter a valid date"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid

	
    submitHandler: function(form) {
      form.submit();
    }
	
			
  });
});


</script>