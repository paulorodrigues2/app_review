<?php

require_once('/commom/database.php');
require_once('/commom/common.php');
require_once('/commom/functions.php');

class feed
{
	 /* News Feed Data */
     public function newsFeed()
     {
        $db = getDB();
		$stmt = $db->prepare("SELECT user.id as id, user.nome as nome, app.id as app_id, app.nome as app_nome, app.descricao as app_descricao, app.like_count as like_count FROM  user , app  WHERE user.id=app.user_id ORDER BY app.id DESC");  
		$stmt->execute();
		//$this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		return $data;
     }

      /* User Reaction Check */
     public function reactionCheck($id, $msg_id)
     {
		try{
        $db = getDB();
		$stmt = $db->prepare("SELECT app_like.id as app_like_id, reaction.name as reaction, reaction.id as reaction_id from app_like, reaction WHERE reaction.id=app_like.reaction_id and app_like.user_id=:id and app_like.app_id=:app_id and app_like.Reaction_id=reaction.id");  
		$stmt->bindValue(':id', $id, PDO::PARAM_INT);
		$stmt->bindValue(':app_id', $msg_id, PDO::PARAM_INT);
		$stmt->execute();
		
		//$this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
		$data= $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		return $data;
		}
		 catch(Exception $e) {
    echo 'Exception -> ';
    var_dump($e->getMessage());
}
     }


      /* News Feed Data */
     public function userReaction($id,$msg_id,$rid)
     {
        $db = getDB();
		$stmt1 = $db->prepare("SELECT app_like.id as app_like_id, app_like.reaction_id as reaction_id FROM app_like WHERE app_like.user_id=:id AND app_like.app_id=:app_id");  
		$stmt1->bindValue(':id', $id, PDO::PARAM_INT);
		$stmt1->bindValue(':app_id', $msg_id, PDO::PARAM_INT);
		$stmt1->bindValue(':reaction_id', $rid, PDO::PARAM_INT);
		$stmt1->execute();
		
		$count=$stmt1->rowCount();

		if($count > 0)
        {
        $stmt = $db->prepare("DELETE FROM  app_like WHERE  app_like.user_id=:id AND app_like.app_id:app_id");  
		$stmt->bindValue(':id', $id, PDO::PARAM_INT);
		$stmt->bindValue(':app_id', $msg_id, PDO::PARAM_INT);
		$stmt->execute();
		
        $db = null;
		return 2;
        }
        else
        {

        $stmt = $db->prepare("INSERT INTO app_like (app_id, user_id, reaction_id) VALUES (:app_id, :id, :rid)");  
		$stmt->bindValue(':id', $id, PDO::PARAM_INT);
		$stmt->bindValue(':app_id', $msg_id, PDO::PARAM_INT);
		/*$created=time();
		$stmt->bindValue(':created', $created, PDO::PARAM_INT);*/
		$stmt->bindValue(':reaction_id', $rid, PDO::PARAM_INT);
		$stmt->execute();
		
		$db = null;
		return 1;

        }

		
     }


}
 ?>